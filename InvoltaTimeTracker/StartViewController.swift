//
//  StartViewController.swift
//  InvoltaTimeTracker
//
//  Created by Денис Савин on 26.03.16.
//  Copyright © 2016 Vladimir Kontsov. All rights reserved.
//

import UIKit
import Foundation
import KeychainSwift

class StartViewController: UIViewController {

    let keychain = KeychainSwift(keyPrefix: "AppTimeTracker_")

    @IBOutlet weak var statusLoad: UIActivityIndicatorView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.statusLoad.startAnimating()
    }

    override func viewDidAppear(animated: Bool) {
        self.checkTocken()
    }
    
    
    func checkTocken() {
        let token = keychain.get("tokenApp")
        
        if token != nil {
            performSegueWithIdentifier("StartAppController", sender: self)
        } else {
            
            performSegueWithIdentifier("StartLoginController", sender: self)
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    


}
